package com.aartek.repository;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.aartek.model.RegistrationView;

@Repository
public class RegistrationRepository {
	@Autowired 
	private HibernateTemplate hibernateTemplate;
	public void registrationSave(RegistrationView registration) {
		System.out.println("registration repository ");
		System.out.println(registration.getFirstName());
		hibernateTemplate.save(registration);
		
	}
	
	public List<RegistrationView> registrationShow(){
		System.out.println("inside show repository");
		List userlist=hibernateTemplate.find("from RegistrationView");
		Iterator iterator=userlist.iterator();
		while(iterator.hasNext()){
			RegistrationView registrationView=(RegistrationView)iterator.next();
		}
		return userlist;
		
	}

}

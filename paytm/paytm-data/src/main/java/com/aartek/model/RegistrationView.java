package com.aartek.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="mvcregistration")
public class RegistrationView {
	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	
	@Column(name="FIRSTNAME")
	private String firstName;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(name="LASTNAME")
	private String lastName;
	
	@Column(name="EMAILID")
	private String emailId;
	
	@Column(name="CONTACT")
	private String contact;
	
	@Column(name="FEES")
	private String fees;
	
	@Column(name="BATCH")
	private String batch;
	
	@Column(name="password")
	private String password;
	
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBatch() {
		return batch;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getFees() {
		return fees;
	}
	public void setFees(String fees) {
		this.fees = fees;
	}
	

}

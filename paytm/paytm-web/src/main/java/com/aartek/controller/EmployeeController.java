package com.aartek.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aartek.model.UserLogin;

	@Controller
	@RequestMapping("/UserLogin/add.htm")
	public class EmployeeController {
	 @RequestMapping(method = RequestMethod.GET)
	 ModelAndView add(HttpServletRequest request, HttpServletResponse response)
	   throws Exception {
	   UserLogin login = new UserLogin();
	  return new ModelAndView("addemployee", "UserLogin", login);

	 }
	}



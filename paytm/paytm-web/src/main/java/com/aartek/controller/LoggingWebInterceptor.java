package com.aartek.controller;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.aartek.model.RegistrationView;
@Component
public class LoggingWebInterceptor extends HandlerInterceptorAdapter
{

@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
	    throws Exception {
		System.out.println("inside prehandler");
		
		/*RegistrationView registration = (RegistrationView) request.getSession().getAttribute("registration");

		if (registration == null || registration.equals("")) {
			response.sendRedirect("login.do");
			return false;
		}*/
		return true;
	}

@Override
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
		
	
		System.out.println("inside posthandler");
	}

@Override
	public void afterCompletion(
			HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
		System.out.println("inside after completion");
	}

}


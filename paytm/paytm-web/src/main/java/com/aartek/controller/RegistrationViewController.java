package com.aartek.controller;

import java.util.List;
import java.util.Map;

//import javax.enterprise.inject.Model;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



import com.aartek.model.RegistrationView;
import com.aartek.services.RegistrationViewService;
@Controller
public class RegistrationViewController {
	@Autowired
	private RegistrationViewService registrationService;
	
	
	@RequestMapping(value="/registration",method=RequestMethod.GET)
	public String showRegistration(Map<String, Object> map, Model model){
		System.out.println();
		List <RegistrationView>list=registrationService.registrationShow();
		//map.put("RegistrationView", new RegistrationView());
		System.out.println("Hello registration controller for display registration");
		map.put("RegistrationView", new RegistrationView());
		model.addAttribute("user",list);
		return"registration";
		
	}
	
	@RequestMapping(value="/Registration",method=RequestMethod.POST)
	public String signUp(@ModelAttribute("RegistrationView") RegistrationView registration){
		System.out.println("hello registration controller registration submitted");
		System.out.println(registration.getFirstName());
		
		//return "registration";
		registrationService.registrationSave(registration);
		//return "redirect:/welcome";
		
		return "redirect:/registration.do";
		
	}

}

package com.aartek.controller;

import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aartek.model.UserLogin;
import com.aartek.services.LoginService;
import com.aartek.validator.RegistrationValidator;


@Controller
public class UserLoginController {
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private RegistrationValidator registrationValidator;
	
	@RequestMapping(value = "/login", method = { RequestMethod.GET })
	public String showLogin(Map<String, Object> map) {
		System.out.println("hello controller");
		map.put("UserLogin", new UserLogin());

		return "login";
	}
	

	@RequestMapping(value = "/loginsubmit",method = RequestMethod.POST)
	public String loginsubmit(@ModelAttribute("UserLogin")UserLogin login, BindingResult result) {
		System.out.println("emailiD:"+login.getEmailId());
		System.out.println("pass"+login.getPassword());
		//return "login";
		
		registrationValidator.validate(login, result);
		if (result.hasErrors()) {
			System.out.println("errors occurred");
			return "login";
		}
			loginService.loginsave(login);
			
		return "welcome";
		
	
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String showWelcome(Map<String,Object> map)
	{
		map.put("UserLogin",new UserLogin());
		return "welcome";
	}


}

package com.aartek.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.aartek.model.UserLogin;

@Component
public class RegistrationValidator 
{
	public boolean supports(Class<?>Clazz){
		return UserLogin.class.isAssignableFrom(Clazz);
		
		
	}
	public void validate(Object target, Errors errors){
		System.out.println("hello validator");
		UserLogin userLogin=(UserLogin) target;
		ValidationUtils.rejectIfEmpty(errors, "emailId", "errors.emailId.empty");
		ValidationUtils.rejectIfEmpty(errors, "password", "errors.password.empty");
		//ValidationUtils.rejectIfEmpty(errors, "firstName", "errors.firstName.empty");
		//ValidationUtils.rejectIfEmpty(errors, "lastName", "errors.lastName.empty");
		/*ValidationUtils.rejectIfEmpty(errors, "contact", "errors.contact.empty");
		ValidationUtils.rejectIfEmpty(errors, "fees", "errors.fees.empty");
		ValidationUtils.rejectIfEmpty(errors, "batch", "errors.batch.empty");*/
	
		if(userLogin.getEmailId() !=null && userLogin.getEmailId().trim().length() > 0){
			boolean b=ValidationUtil.validateEmail(userLogin.getEmailId());
			if (userLogin.getEmailId().contains("@") != true && !b) {
				errors.rejectValue("emailId", "errors.email.first.rule");
			} else if (userLogin.getEmailId().contains(".com") != true
					&& userLogin.getEmailId().contains(".net") != true
					&& userLogin.getEmailId().contains(".co.in") != true && !b) {
				errors.rejectValue("emailId", "errors.email.second.rule");
			} else if (!b) {
				errors.rejectValue("emailId", "errors.email.required");
			}
		
		}
		
	}

}

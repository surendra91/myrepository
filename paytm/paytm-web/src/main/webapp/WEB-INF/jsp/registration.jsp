<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<form:form method="POST" action="Registration.do" modelAttribute="RegistrationView" autocomplete="off">
        <table width="100%" border="0">

          <tr>
            <td><div class="form-control">
                <form:errors path="firstName"  class="label error-label"></form:errors> 
                <label>
                  <p>
                    First Name<small class="required">*</small>
                  </p> <form:input path="firstName" class="inputControl" placeholder="First Name" 
                    maxlength="30" onkeypress="return Alphabets(event)" /> </label>
              </div>
            </td>
            <td><div class="form-control">
                <form:errors path="lastName"  class="label error-label"></form:errors> 
                <label>
                  <p>
                    Last Name<small class="required"></small>
                  </p> <form:input path="lastName" class="inputControl" placeholder="Last Name"
                    maxlength="30" onkeypress="return Alphabets(event)" /> </label>
              </div>
            </td>
          </tr>
          <tr>
            <td><div class="form-control">
                <form:errors path="emailId"  class="label error-label"></form:errors> 
                <label>
                  <p>
                    Email Id<small class="required">*</small>
                  </p> <form:input path="emailId" class="inputControl" placeholder="Email Id"
                    maxlength="100" onblur="emailVarification(this);" /> </label>
              </div>
            </td>
            
              <td><div class="form-control">
                <form:errors path="password"  class="label error-label"></form:errors> 
                <label>
                  <p>
                    Password<small class="required"></small>
                  </p> <form:input path="password" class="inputControl" placeholder="password"
                     maxlength="20" onkeypress="return Alphabets(event)" /> </label>
              </div>
            </td>
          </tr> 
           
          <tr>
            <td><div class="form-control">
                <form:errors path="contact"  class="label error-label"></form:errors> 
                <label>
                  <p>
                    Contact<small class="required">*</small>
                  </p> <form:input path="contact" class="inputControl" placeholder="Mobile Number"
                    maxlength="11" onkeypress="return onlyNos(event,this);" /> </label>
              </div>
            </td>
            <td><div class="form-control">
                 <form:errors path="fees"  class="label error-label"></form:errors> 
                <label>
                  <p>
                    Fees<small class="required"></small>
                  </p> <form:input path="fees" class="inputControl" placeholder="fees"
                     maxlength="20" onkeypress="return Alphabets(event)" /> </label>
              </div>
            </td>
          </tr> 
          
          
          
          <td><div class="form-control">
               <form:errors path="batch"  class="label error-label"></form:errors> 
                <label>
                  <p>
                    Batch<small class="required"></small>
                  </p> <form:input path="batch" class="inputControl" placeholder="fees"
                    maxlength="20" onkeypress="return Alphabets(event)" /> </label>
              </div>
            </td>
          </tr> 
          <tr>
            <td colspan="2"><input type="submit" value="Submit" class="btn lg-btn" onclick="remeberMe();" /></td>
           
          </tr>
         </table> 
          
           <table width="100%" border="0" bgcolor="#5e809f

           ">
           <tr>
           <td>FIRSTNAME</td>
           <td>LASTNAME</td>
           <td>EMAILID</td>
           <td>PASSWORD</td>
           <td>CONTACT</td>
           <td>FEES</td>
           <td>BATCH</td>
           </tr>
           <c:forEach items="${user}" var="user">
           <tr>
           <td>${user.firstName}</td>
           <td>${user.lastName}</td>
           <td>${user.emailId}</td>
           <td>${user.contact}</td>
           <td>${user.fees}</td>
           <td>${user.batch}</td>
          <td>${user.password}</td> 
         </tr>
         </c:forEach>
         </table>
         

</form:form>

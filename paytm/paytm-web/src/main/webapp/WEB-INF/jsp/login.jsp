<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<a href="?lang=en" class="btn btn-sm btn-success">English</a> <a href="?lang=fr" class="btn btn-sm btn-success">French</a>
<form:form method="POST" action="loginsubmit.do" modelAttribute="UserLogin" autocomplete="off">
 
        <table width="100%" border="0">
          <tr>
            <td><div class="form-control">
                <form:errors path="emailId"  class="label error-label"></form:errors> 
                <label>
                  <p>
                   <spring:message code="label.username"></spring:message>
                   UserName <small class="required"></small>
                  </p> <form:input path="emailId" class="inputControl inptlogin" placeholder="Please enter your email"
                     maxlength="50" id="emailId" /> </label>
                    
                    <%--  <spring:message code="label.username"></spring:message> --%>
              </div></td>
          </tr>
          <tr>
            <td><div class="form-control">
                <form:errors path="password"  class="label error-label"></form:errors> 
                <label>
                  <p>
                  <spring:message code="label.password"></spring:message>
                 Password  <small class="required"></small>
                  </p> <form:password path="password" class="inputControl inptlogin" placeholder="Password" 
                    maxlength="15" id="password" /> </label>
                    <%-- <spring:message code="label.password"></spring:message> --%>
              </div></td>
          </tr>          
            <tr>
            <td><div class="form-control">
                <label>
                   <small class="required"></small>
                 <input type="checkbox" id="rememberMe" /> Remember Me</label>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <label>
                  
                  <a href="forgotPassword.do" >Forgot Password</a></label> 
             
              </div></td>
          </tr>                  
           <tr>
           <!--  <td colspan="2"><input type="submit" value="submit" class="btn lg-btn" onclick="remeberMe();" /></td>
           -->
            <spring:message code="label.submit" var="labelSubmit"></spring:message>
                <td colspan="2"><input type="submit" value="submit"/></td>
          </tr>
          <tr>
         
          </tr>
          
			
          
          </table>
          </form:form>
          

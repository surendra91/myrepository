package com.aartek.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aartek.model.RegistrationView;
import com.aartek.repository.RegistrationRepository;

@Service
public class RegistrationViewService {
	@Autowired 
	private RegistrationRepository registrationRepository;
	
	public void registrationSave(RegistrationView registration) {
		System.out.println("hello registration services for save");
		System.out.println(registration.getFirstName());
		registrationRepository.registrationSave(registration);
		
	}
	public List<RegistrationView> registrationShow(){
		System.out.println("inside show services");
		List<RegistrationView> userlist=registrationRepository.registrationShow();
		
		return userlist;
		
	}
	
	
}
